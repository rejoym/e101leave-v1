<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->string('leave_reason', 250);
            $table->text('description');
            $table->date('from');
            $table->date('to');
            $table->integer('type_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->enum('status', ['approved', 'declined']);
            $table->timestamps();
        });
        Schema::table('leaves', function (Blueprint $table) {
            $table->foreign('type_id')
                  ->references('id')
                  ->on('leave_types')
                  ->onUpdate('cascade');
            $table->foreign('employee_id')
                  ->references('id')
                  ->on('employees')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves_types');
        Schema::dropIfExists('leaves');
    }
}
