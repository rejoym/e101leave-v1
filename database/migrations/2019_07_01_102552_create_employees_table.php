<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('address', 100);
            $table->string('mobile_no', 100);
            // $table->integer('user_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('employees', function (Blueprint $table) {
            // $table->foreign('user_id')
            //       ->references('id')
            //       ->on('users')
            //       ->onUpdate('cascade');
            $table->foreign('department_id')
                  ->references('id')
                  ->on('departments')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('departments');
        Schema::dropIfExists('employees');
    }
}
