<!DOCTYPE html>
<html>
  <head>
    <title>E-101Leave</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- styles -->
    <link href="{{asset('assets/css/styles.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">


  <div class="page-content container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-wrapper">
              <div class="box">
                  <div class="content-wrap">
                    <form action="" method="post">
                      {!!csrf_field()!!}
                      <h6>Sign Up</h6>
                      <input class="form-control" type="text" placeholder="E-mail address" name="input-email">
                      <span class="my-error">{{$errors->first('input-email')}}</span>
                      <input class="form-control" type="password" placeholder="Password" name="input-password">
                      <span class="my-error">{{$errors->first('input-password')}}</span>
                      <input type="text" class="form-control" placeholder="Username" name="input-username">
                      <span class="my-error">{{$errors->first('input-username')}}</span>
                      <select class="form-control" name="input-role">
                        <option value="">Select Role</option>
                        <option value="admin">Admin</option>
                        <option value="employee">Employee</option>
                      </select>
                      <span class="my-error">{{$errors->first('input-role')}}</span>
                      <div class="action">
                          <button class="btn btn-primary">Sign Up</a>
                      </div>
                    </form>
                  </div>
              </div>

              <div class="already">
                  <p>Have an account already?</p>
                  <a href="{{'login'}}">Login</a>
              </div>
          </div>
      </div>
    </div>
  </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/custom.js')}}"></script>
  </body>
</html>