<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>
<body>
<h1>Welcome,{{ $user->username}}</h1>
<br/>
<div>
  <p>Thank you for creating an account with  E-101 Leave. Please follow the link below to verify your email address</p>
</div>
<a href="{{url('user/verify/' .$user->token)}}">Verify Email</a>
<div>
  <p>If you didn't register in E-101 Leave, please ignore this mail</p>
</div>
</body>

</html>