@extends('backend._shared.layouts.master')
@section('main-content')
@if(isset($data['edit_details']))
  <?php $edit_flag = true; ?>
@endif
<div class="col-md-10" style="height:550px">
    @if(Session::get('Success'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! New Department has been successfully registered.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if(Session::get('SuccessEdit'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! Department has been successfully updated.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
              <div class="content-box-large">
                <div class="panel-heading">
                      <div class="panel-title">{{ isset($edit_flag) ? 'Edit' : 'Create'}} Department</div>
                  </div>
                <div class="panel-body">
                  <form action="@if(isset($edit_flag)){{route('post.department.edit')}}@endif" method="post">
                    {!!csrf_field()!!}
                  <fieldset>
                    <div class="form-group">
                      <label>Department Name</label>
                      <input class="form-control" placeholder="Department Name" type="text" name="input-dept-name" value="{{isset($edit_flag) ? $data['edit_details']->department_name : ''}}">
                      <span class="my-error">{{$errors->first('input-dept-name')}}</span>
                    </div>
                    <div class="form-group">
                      <label>Department Role</label>
                      <input class="form-control" placeholder="Department Role" type="text" name="input-dept-role" value="{{isset($edit_flag) ? $data['edit_details']->department_role : ''}}">
                      <span class="my-error">{{$errors->first('input-dept-role')}}</span>
                    </div>
                    <div class="form-group">
                      <label>Description</label>
                      <textarea class="form-control" placeholder="Department Description" rows="3" name="input-description">{{isset($edit_flag) ? $data['edit_details']->description : ''}} </textarea>
                      <span class="my-error">{{$errors->first('input-description')}}</span>
                    </div>
                    <div class="form-group">
                      <input type="hidden" value="{{isset($edit_flag) ? $data['edit_details']->id : ''}}" name="input-id">
                    </div>

                  </fieldset>
                  <div>
                    <button class="btn btn-primary">
                      <i class="fa fa-save"></i>
                      {{isset($edit_flag) ? 'Update' : 'Save' }}
                    </button>
                  </div>
                </form>
                </div>
              </div>
            </div>
@stop