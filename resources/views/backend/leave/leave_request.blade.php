@extends('backend._shared.layouts.master')
@section('main-content')
<div class="col-md-10" style="height:550px;">
  @if(Session::get('SuccessDelete'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! Leave Request has been successfully deleted.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
            <div class="content-box-large">
              <!-- <button class="btn btn-primary"><a href="{{route('get.holiday.create')}}" style="color:white;text-decoration: none;">Add Holiday</a></button> -->
              <div class="panel-heading">
              <div class="panel-title">Leave Request</div>


            </div>
              <div class="panel-body">
                <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>S.NO</th>
                          <th>EMPLOYEE NAME</th>
                          <th>LEAVE REASON</th>
                          <th>DESCRIPTION</th>
                          <th>FROM</th>
                          <th>TO</th>
                          <th>TYPE</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php $sn = 0; ?>
                          @foreach($data['all_leave'] as $leaves)
                          <td>{{++$sn}}</td>
                          <td>{{$leaves->employee->first_name}}</td>
                          <td>{{$leaves->leave_reason}}</td>
                          <td>{{$leaves->description}}</td>
                          <td>{{$leaves->from}}</td>
                          <td>{{$leaves->to}}</td>
                          <td>{{$leaves->leaveType->leave_type}}</td>
                          <td><button class="btn btn-success btn-xs">Approve</button>|<button class="btn btn-danger btn-xs">Reject</button>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
              </div>
            </div>
          </div>

@stop