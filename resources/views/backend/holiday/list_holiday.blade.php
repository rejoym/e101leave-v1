@extends('backend._shared.layouts.master')
@section('main-content')
<div class="col-md-10" style="height:550px;">
  @if(Session::get('SuccessDelete'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! Holiday has been successfully deleted.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
            <div class="content-box-large">
              <button class="btn btn-primary"><a href="{{route('get.holiday.create')}}" style="color:white;text-decoration: none;">Add Holiday</a></button>
              <div class="panel-heading">
              <div class="panel-title">Holiday List</div>

              <div class="panel-options">
                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
              </div>
            </div>
              <div class="panel-body">
                <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>S.NO</th>
                          <th>HOLIDAY NAME</th>
                          <th>DESCRIPTION</th>
                          <th>HOLIDAY DATE</th>
                          <th>ACTION</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php $sn = 0; ?>
                          @foreach($data['all_holiday'] as $holidays)
                          <td>{{++$sn}}</td>
                          <td>{{$holidays->name}}</td>
                          <td>{{$holidays->description}}</td>
                          <td>{{$holidays->holiday_date}}</td>
                          <td><a href="{{route('get.holiday.edit', ['id' => $holidays->id]) }}"><span class="glyphicon glyphicon-edit" style="color:red;"></span></a> | <a href="{{route('get.holiday.delete', ['id' => $holidays->id])}}"><span class="glyphicon glyphicon-trash" style="color:red;"></span> </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
              </div>
            </div>
          </div>

@stop