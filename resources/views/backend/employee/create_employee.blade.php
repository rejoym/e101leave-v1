@extends('backend._shared.layouts.master')
@section('main-content')
@if(isset($data['edit_details']))
  <?php $edit_flag = true; ?>
@endif
<div class="col-md-10" style="height:550px">
    @if(Session::get('Success'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! New Employee has been successfully registered.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if(Session::get('SuccessEdit'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! Employee has been successfully updated.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
              <div class="content-box-large">
                <div class="panel-heading">
                      <div class="panel-title">{{ isset($edit_flag) ? 'Edit' : 'Create'}} Employee</div>
                  </div>
                <div class="panel-body">
                  <form action="@if(isset($edit_flag)){{route('post.employee.edit')}}@endif" method="post">
                    {!!csrf_field()!!}
                  <fieldset>
                    <div class="form-group">
                      <label>First Name</label>
                      <input class="form-control" placeholder="First Name" type="text" name="input-first-name" value="{{isset($edit_flag) ? $data['edit_details']->first_name : ''}}">
                      <span class="my-error">{{$errors->first('input-first-name')}}</span>
                    </div>
                    <div class="form-group">
                      <label>Last Name</label>
                      <input class="form-control" placeholder="Last Name" type="text" name="input-last-name" value="{{isset($edit_flag) ? $data['edit_details']->last_name : ''}}">
                      <span class="my-error">{{$errors->first('input-last-name')}}</span>
                    </div>
                    <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control" placeholder="Employee Address" rows="3" name="input-address">{{isset($edit_flag) ? $data['edit_details']->address : ''}} </textarea>
                      <span class="my-error">{{$errors->first('input-address')}}</span>
                    </div>
                    <div class="form-group">
                      <label>Mobile No</label>
                      <input class="form-control" placeholder="Mobile No" type="text" name="input-mobile-no" value="{{isset($edit_flag) ? $data['edit_details']->mobile_no : ''}}">
                      <span class="my-error">{{$errors->first('input-mobile-no')}}</span>
                    </div>
                    <div class="form-group">
                      <label>Department</label>
                      <select class="form-control" name="input-department">
                        <option value="">Select Department</option>
                        @foreach($data['all_department'] as $departments)
                          <option value="{{$departments->id}}"{{(isset($edit_flag) && $data['edit_details']->id == $departments->id) ? 'selected' : '' }}>{{$departments->department_name}}</option>
                        @endforeach
                      </select>
                      <span class="my-error">{{$errors->first('input-department')}}</span>
                    </div>
                    <div class="form-group">
                      <input type="hidden" value="{{isset($edit_flag) ? $data['edit_details']->id : ''}}" name="input-id">
                    </div>

                  </fieldset>
                  <div>
                    <button class="btn btn-primary">
                      <i class="fa fa-save"></i>
                      {{isset($edit_flag) ? 'Update' : 'Save' }}
                    </button>
                  </div>
                </form>
                </div>
              </div>
            </div>
@stop