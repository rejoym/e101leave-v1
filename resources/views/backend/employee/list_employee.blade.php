@extends('backend._shared.layouts.master')
@section('main-content')
<div class="col-md-10" style="height:550px;">
  @if(Session::get('SuccessDelete'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! Employee has been successfully deleted.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
            <div class="content-box-large">
              <button class="btn btn-primary"><a href="{{route('get.employee.create')}}" style="color:white;text-decoration: none;">Add Employee</a></button>
              <div class="panel-heading">
              <div class="panel-title">Employee List</div>

              <div class="panel-options">
                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
              </div>
            </div>
              <div class="panel-body">
                <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>S.NO</th>
                          <th>FIRST NAME</th>
                          <th>LAST NAME</th>
                          <th>ADDRESS</th>
                          <th>MOBILE NO</th>
                          <th>DEPARTMENT</th>
                          <th>ACTION</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php $sn = 0; ?>
                          @foreach($data['all_employee'] as $employees)
                          <td>{{++$sn}}</td>
                          <td>{{$employees->first_name}}</td>
                          <td>{{$employees->last_name}}</td>
                          <td>{{$employees->address}}</td>
                          <td>{{$employees->mobile_no}}</td>
                          <td>{{$employees->department->department_name}}</td>
                          <td><a href="{{route('get.employee.edit', ['id' => $employees->id]) }}"><span class="glyphicon glyphicon-edit" style="color:red;"></span></a> | <a href="{{route('get.employee.delete', ['id' => $employees->id])}}"><span class="glyphicon glyphicon-trash" style="color:red;"></span> </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
              </div>
            </div>
          </div>

@stop