@extends('backend._shared.layouts.master')
@section('main-content')
<div class="col-md-10">
  <div class="row">
    <div class="col-md-12 panel-warning">
      <div class="content-box-header panel-heading">
        <div class="panel-title ">Dashboard</div>

        <div class="panel-options">
          <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
          <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
        </div>
      </div>
      <div class="content-box-large box-with-header">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        <br><br>
      </div>
    </div>
  </div>
  <!-- </div> -->
  <div class="row">
    @if(Auth::user()->user_role == 'admin')
    <div class="col-md-12">
      <div class="content-box-large">
        <div class="panel-heading">
          <div class="panel-title">Leave Requests</div>


        </div>
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>S.NO</th>
                <th>EMPLOYEE NAME</th>
                <th>LEAVE FROM</th>
                <th>LEAVE UPTO</th>
                <th>STATUS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    @else
    <div class="col-md-6">
      <div class="content-box-large">
        <div class="panel-heading">
          <div class="panel-title">Apply for Leave</div>

                      <!-- <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                        <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                      </div> -->
                    </div>
                    <div class="panel-body">
                      <form action="">
                        <fieldset>
                          <div class="form-group">
                            <label>Employee name</label>
                            <select class="form-control">
                              <option value="">Select Employee</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Department</label>
                            <select class="form-control">
                              <option value="">Select Department</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Leave Day Type</label>
                            <select class="form-control">
                              <option value="">Select Leave Day Type</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Leave Type</label>
                            <select class="form-control">
                              <option value="">Select Leave Type</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>From</label>
                            <input type="date" name="input-from" class="form-control">
                          </div>
                          <div class="form-group">
                            <label>To</label>
                            <input type="date" name="input-from" class="form-control">
                          </div>
                        </fieldset>
                        <div>
                          <div class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Submit
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                @endif
                <!-- </div> -->
                <div class="col-md-6">
                  <div class="content-box-large">
                    <div class="panel-heading">
                      <div class="panel-title">Upcoming Holidays</div>
                    </div>
                    <div class="panel-body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>S.NO</th>
                            <th>HOLIDAY</th>
                            <th>DESCRIPTION</th>
                            <th>DATE</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php $sn = 0; ?>
                            @foreach($data['all_holiday'] as $holidays)
                            <td>{{++$sn}}</td>
                            <td>{{$holidays->name}}</td>
                            <td>{{$holidays->description}}</td>
                            <td>{{$holidays->holiday_date}}</td>
                          </tr>
                          @endforeach

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="content-box-large">
                    <div class="panel-heading">
                      <div class="panel-title">Employees</div>
                    </div>
                    <div class="panel-body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>S.NO</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>ADDRESS</th>
                            <th>MOBILE NO</th>
                            <th>DEPARTMENT</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php $sn = 0; ?>
                            @foreach($data['all_employee'] as $employees)
                            <td>{{++$sn}}</td>
                            <td>{{$employees->first_name}}</td>
                            <td>{{$employees->last_name}}</td>
                            <td>{{$employees->address}}</td>
                            <td>{{$employees->mobile_no}}</td>
                            <td>{{$employees->department->department_name}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              @stop