<!DOCTYPE html>
<html>
<head>
  <title>E-101Leave</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- styles -->
  <link href="{{asset('assets/css/styles.css')}}" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
    <!-- <div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-12">
                <div class="logo">
                   <h1><a href="index.html">e101Leave Admin</a></h1>
                </div>
             </div>
          </div>
       </div>
     </div> -->

     <div class="page-content container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-wrapper">
            <div class="box">
              @if(Session::get('loggedOut'))
              <p style="color:green;"><i class="fa fa-check"></i> You have been successfully logged out!</p>
              @endif
              <div class="content-wrap">
                <h6>Sign In</h6>
                <form action="" method="post">
                  {!!csrf_field()!!}
                  @if($errors->has('useroremail') || $errors->has('password'))
                  <label style="color:red;"> Email and password fields are required!</label>
                  @endif
                  @if(Session::get('loginError'))
                  <label style="color:red;">The credentials you supplied were not correct!</label>
                  @endif
                  <input class="form-control" name="useroremail" type="text" placeholder="E-mail address or username">
                  <input class="form-control" name="password" type="password" placeholder="Password">
                  <div class="action">
                    <button class="btn btn-primary signup">Login</button>
                  </div>
                </form>
              </div>
            </div>

            <div class="already">
              <p>Don't have an account yet?</p>
              <a href="{{url('/user/signup')}}">Sign Up</a>
            </div>
          </div>
        </div>
      </div>
    </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
  </html>