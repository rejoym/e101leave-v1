@include('.backend._shared.layouts.header')
@include('backend._shared.layouts.leftnav')
@yield('main-content')
@include('.backend._shared.layouts.footer')