<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = ['leave_reason', 'description', 'from', 'to', 'leave_type_id', 'employee_id'];

    /**
     * function to establish relation between leaves and leavetype
     * @return [type] [description]
     */
    public function leaveType()
    {
      return $this->belongsTo('App\Models\Leave_type', 'leave_type_id', 'id');
    }


    public function employee()
    {
      return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }
}
