<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyUserByEmail extends Model
{
    /**
     * arrays or fields that are mass assignable
     * @var [type]
     */
    protected $fillable = ['token', 'user_id'];

    /**
     * one to many relation between verify_user and customer_user
     * @return [array] [description]
     */
    public function users()
    {
      return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
