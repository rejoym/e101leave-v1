<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['department_name', 'department_role', 'description'];


    public function employees()
    {
      return $this->hasMany('App\Models\Employee');
    }
}
