<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['first_name', 'last_name', 'address', 'mobile_no', 'user_id', 'department_id'];

    /**
     * function to establish relation between department and employee
     * @return [type] [description]
     */
    public function user()
    {
      return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * function to establish relation between department and employee
     * @return [type] [description]
     */
    public function department()
    {
      return $this->belongsTo('App\Models\Department', 'department_id', 'id');
    }


}
