<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leave_type extends Model
{
    protected $fillable = ['leave_type'];
}
