<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostUserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-username' => 'required|max:255|unique:users,username',
            'input-email' => 'required|email|max:255|unique:users,email',
            'input-password' => 'required|min:6',
            'input-role' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'input-username.required' => 'Please enter a username',
            'input-email.required' => 'Please enter an email address',
            'input-email.email' => 'Please enter a valid email address',
            'input-role.required' => 'Please select role'
        ];
    }
}
