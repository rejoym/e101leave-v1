<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-dept-name' => 'required',
            'input-dept-role' => 'required',
            'input-description' => 'required'
        ];
    }

    /**
     * Display custom messages
     * @return [type] [description]
     */
    public function messages()
    {
        return [
            'input-dept-name.required' => 'Please enter department name',
            'input-dept-role.required' => 'Please enter department role',
            'input-description.required' => 'Please enter department description'
        ];
    }
}
