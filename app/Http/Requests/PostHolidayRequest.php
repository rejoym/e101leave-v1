<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostHolidayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-holiday-name' => 'required',
            'input-description' => 'required',
            'input-date' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'input-holiday-name.required' => 'Please specify holiday name',
            'input-description.required' => 'Description is required',
            'input-date.required' => 'Holiday date is required'
        ];
    }
}
