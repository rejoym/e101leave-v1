<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-first-name' => 'required',
            'input-last-name' => 'required',
            'input-address' => 'required',
            'input-mobile-no' => 'required',
            'input-department' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'input-first-name.required' => 'Please enter first name',
            'input-last-name.required' => 'Please enter last name',
            'input-address.required' => 'Please enter address',
            'input-mobile-no.required' => 'Please enter mobile no',
            'input-department.required' => 'Please enter department'
        ];
    }
}
