<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostDepartmentRequest;
use App\Models\Department as Department;

class DepartmentController extends Controller
{

    private $department_model;
    public function getIndex()
    {
      return view('backend.department.create_department');
    }

    /**
     * function to handle post request of department
     * @param  PostDepartmentRequest $request [description]
     * @return [type]                         [description]
     */
    public function postDepartment(PostDepartmentRequest $request)
    {
      $dept_name = $request->input('input-dept-name');
      $dept_role = $request->input('input-dept-role');
      $description = $request->input('input-description');
      try
      {
        Department::create([
          'department_name' => $dept_name,
          'department_role' => $dept_role,
          'description' => $description
        ]);
        $request->session()->flash('Success');
        return redirect()->route('get.department.create');

      }
      catch(\Exception $e)
      {
        dd($e);
        return redirect()->back()->withErrors('Error');
      }
    }


    public function getDepartmentList()
    {
      $data['all_department'] = Department::all();
      return view('backend.department.list_department', compact('data'));
    }


    public function getEditDepartment($id)
    {
      $data['edit_details'] = Department::find($id);
      return view('backend.department.create_department', compact('data'));
    }


    public function postEditDepartment(PostDepartmentRequest $request)
    {
      $id = $request->input('input-id');
      $dept = Department::find($id);
      $dept_name = $request->input('input-dept-name');
      $dept_role = $request->input('input-dept-role');
      $dept_description = $request->input('input-description');
      $dept->department_name = $dept_name;
      $dept->department_role = $dept_role;
      $dept->description = $dept_description;
      $dept->save();
      $request->session()->flash('SuccessEdit');
      return redirect()->route('get.department.create');

    }


    public function deleteDepartment(Request $request, $id)
    {
      $department = Department::find($id);
      $department->delete();
      $request->session()->flash('SuccessDelete');
      return redirect()->route('get.department.list');
    }
}
