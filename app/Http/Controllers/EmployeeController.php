<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department as Department;
use App\Models\Employee as Employee;
use App\Http\Requests\PostEmployeeRequest;

class EmployeeController extends Controller
{

    private $employee_model;
    /**
     * [getIndex description]
     * @return [type] [description]
     */
    public function getIndex()
    {
      $data['all_department'] = Department::all();
      return view('backend.employee.create_employee', compact('data'));
    }

    /**
     * function to handle post request of employee
     * @param  PostEmployeeRequest $request [description]
     * @return [type]                       [description]
     */
    public function postEmployee(PostEmployeeRequest $request)
    {
      $fname = $request->input('input-first-name');
      $lname = $request->input('input-last-name');
      $address = $request->input('input-address');
      $mobile = $request->input('input-mobile-no');
      $dept = $request->input('input-department');
      try
      {
        $insert_into_employees = Employee::create([
          'first_name' => $fname,
          'last_name' => $lname,
          'address' => $address,
          'mobile_no' => $mobile,
          'department_id' => $dept
        ]);
        $request->session()->flash('Success');
        return redirect()->route('get.employee.create');
      }
      catch(\Excepion $e)
      {
        dd($e);
        return redirect()->back()->withErrors('Error');
      }
    }

    /**
     * function to get employee list
     * @return [type] [description]
     */
    public function getEmmployeeList()
    {
      $data['all_employee'] = Employee::all();
      return view('backend.employee.list_employee', compact('data'));
    }

    /**
     * function to load edit employee page
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getEditEmployee($id)
    {
      $data['all_department'] = Department::all();
      $data['edit_details'] = Employee::find($id);
      return view('backend.employee.create_employee', compact('data'));
    }

    /**
     * function to handle post edit employee
     * @param  PostEmployeeRequest $request [description]
     * @return [type]                       [description]
     */
    public function postEditEmployee(PostEmployeeRequest $request)
    {
      $id = $request->input('input-id');
      $employee = Employee::find($id);
      $employee->first_name = $request->input('input-first-name');
      $employee->last_name = $request->input('input-last-name');
      $employee->address = $request->input('input-address');
      $employee->mobile_no = $request->input('input-mobile-no');
      $employee->department_id = $request->input('input-department');
      $employee->save();
      $request->session()->flash('SuccessEdit');
      return redirect()->route('get.employee.create');
    }


    public function deleteEmployee(Request $request, $id)
    {
      $employee = Employee::find($id);
      $employee->delete();
      $request->session()->flash('SuccessDelete');
      return redirect()->route('get.employee.list');
    }
}
