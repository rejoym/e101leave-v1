<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leave as Leave;

class LeaveController extends Controller
{
    public function getIndex()
    {
      $data['all_leave'] = Leave::all();
      return view('backend.leave.leave_request', compact('data'));
    }
}
