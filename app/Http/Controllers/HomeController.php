<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee as Employee;
use App\Models\Holiday as Holiday;

class HomeController extends Controller
{
    /**
     * function which returns the landing page
     * @return [type] [description]
     */
    public function getIndex()
    {
      $data['all_employee'] = Employee::all();
      $data['all_holiday'] = Holiday::all();
      return view('backend.admin.home', compact('data'));
    }
}
