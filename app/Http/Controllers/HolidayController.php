<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostHolidayRequest;
use App\Models\Holiday as Holiday;

class HolidayController extends Controller
{

    public function getIndex()
    {
      return view('backend.holiday.create_holiday');
    }


    public function postHoliday(PostHolidayRequest $request)
    {
      $name = $request->input('input-holiday-name');
      $description = $request->input('input-description');
      $date = $request->input('input-date');
      try
      {
        $insert_into_holiday = Holiday::create([
          'name' => $name,
          'description' => $description,
          'holiday_date' => $date
        ]);
        $request->session()->flash('Success');
        return redirect()->route('get.holiday.create');
      }
      catch(\Exception $e)
      {
        dd($e);
        return redirect()->back()->withErrors('Error');
      }
    }

    /**
     * function to get list of holidays
     * @return [type] [description]
     */
    public function getHolidayList()
    {
      $data['all_holiday'] = Holiday::all();
      return view('backend.holiday.list_holiday', compact('data'));
    }


    public function getEditHoliday($id)
    {
      $data['edit_details'] = Holiday::find($id);
      return view('backend.holiday.create_holiday', compact('data'));
    }


    public function postEditHoliday(PostHolidayRequest $request)
    {
      $id = $request->input('input-id');
      $holiday = Holiday::find($id);
      $holiday->name = $request->input('input-holiday-name');
      $holiday->description = $request->input('input-description');
      $holiday->holiday_date = $request->input('input-date');
      $holiday->save();
      $request->session()->flash('SuccessEdit');
      return redirect()->route('get.holiday.create');

    }


    public function deleteHoliday(Request $request, $id)
    {
      $holiday = Holiday::find($id);
      $holiday->delete();
      $request->session()->flash('SuccessDelete');
      return redirect()->route('get.holiday.list');
    }
}
