<?php

namespace App\Http\Controllers\Auth;

use \Auth;
use DB;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('getLogout');
    }

    public function getIndex()
    {
        return view('backend.login');
    }


    public function postLogin(Request $request)
    {
        $request->validate([
            'useroremail' => 'required',
            'password' => 'required',
        ]);
        $usernameoremail = $request->input('useroremail');
        $password = $request->input('password');
        $remember = $request->input('remember-me') ? true : false;
        if( (Auth::attempt(['username' => $usernameoremail, 'password' => $password], $remember)) || (Auth::attempt(['email' => $usernameoremail, 'password' => $password], $remember)) )
        {
            $this->loggedIn();
        }

        $request->session()->flash('loginError', 'Invalid credentials!!!');
        return redirect()->route('login')->withInput($request->except('password'));
    }

    /**
     * function to check user info
     * @return [view] [description]
     */
    public function loggedIn()
    {
        $user_info = $this->loggedInInfo();
        Session::put('user_info', $user_info);
        return redirect()->intended('home');
    }

    /**
     * check if user exists
     * @return [view|array] [description]
     */
    public function loggedInInfo()
    {
        $id = Auth::id();
        $user_info_detail[] = DB::table('users')
                                ->select('*')
                                ->where('id', $id)
                                ->first();
        if(empty($user_info_detail))
        {
            Session::flash('loginErr', 'User Not Found!!');
            return redirect()->route('login');
        }
        return $user_info_detail;
    }


     public function getLogout(Request $request) {
        $user = Auth::user();
        if($user):
            Auth::logout();
            $request->session()->flash('loggedOut', 'You have sucessfully logged out.');
            $request->session()->forget('user_info');
        endif;

        return redirect()->route('login');
    }

}
