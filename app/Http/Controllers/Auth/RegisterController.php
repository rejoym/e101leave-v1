<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\VerifyUserByEmail as VerifyUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\PostUserRegisterRequest;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function getIndex()
    {
        return view('backend.register');
    }

    public function postRegister(PostUserRegisterRequest $request)
    {
      try
      {
        $uname = $request->input('input-username');
        $email = $request->input('input-email');
        $psw = $request->input('input-password');
        $role = $request->input('input-role');
        $data_user = [
          'email' => $email,
          'password' => bcrypt($psw),
          'username' => $uname,
          'user_role' => $role
        ];

        $register_user = $this->createUser($data_user);

        $check_send = false;

        $uid = $register_user['id'];
        $verifyUser = VerifyUser::create([
          'user_id' => $uid,
          'token' => str_random(40)
        ]);
        $check_send = $this->sendEmail($uid);
        if($check_send === true) {
          $request->session()->flash('Success', 'New User Registered Successfully!! Please activate your account from the link sent in your email');
          return redirect()->route('login');
        }
        // else {
        //   echo "error";
        // }
      }catch(\Exception $e) {
        // report($e);
        dd($e);
        // return redirect()->back();
      }
    }

    /**
     * function to send email
     * @return [boolean] [returns true if mail successfully sent else return false]
     */
    public function sendEmail($id)
    {
      try{
        $data = VerifyUser::with('users')->get()->find($id);
        Mail::send('backend.verifyUsers', ['user' => $data], function($message) use($data){
          $message->to($data['users']['email'], $data['users']['username']);
          $message->subject("Activation Code");
        });
        return true;
      }
      catch(\Exception $e){
        // report($e);
        dd($e);
        // return false;
      }

    }

    /**
     * function to verify the user
     * @param  [string] $token [token of each individual user]
     * @return [route]        [description]
     */
    public function verifyUser($token)
    {
      $verify = VerifyUser::where('token', $token)->first();
      if($verify) {
        //variable to store all the users from customer_user table defined in VerifyUser table
        $user = $verify->users;
        if(!$user->email_verified) {
          $verify->users->email_verified = 1;
          $verify->users->save();
          $status = "Your email is verified. Account Activated";
        }
        else {
          $status = "Account Already Verfied !";
        }
      }
      else {
        return redirect()->route('get.customer.login')->with('warning', "Sorry! Your email could not be identified");
      }
      return redirect()->route('get.customer.login')->with('status', $status);
    }


    public function createUser(array $data)
    {
      return User::create($data);
    }

}
