<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend.login');
});
Route::prefix('user')->group( function() {
  Route::get('/login', ['uses' => 'Auth\LoginController@getIndex', 'as' => 'login']);
  Route::post('/login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'post.login']);
  Route::get('/signup', ['uses' => 'Auth\RegisterController@getIndex', 'as' => 'get.signup']);
  Route::post('/signup', ['uses' => 'Auth\RegisterController@postRegister', 'as' => 'post.signup']);
  Route::get('/logout', ['uses' => 'Auth\LoginController@getLogout', 'as' => 'get.logout']);

  Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', ['uses' => 'HomeController@getIndex', 'as' => 'get.home']);
    Route::get('/department/create', ['uses' => 'DepartmentController@getIndex', 'as' => 'get.department.create']);
    Route::post('/department/create', ['uses' => 'DepartmentController@postDepartment', 'as' => 'post.department.create']);
    Route::get('/department/list', ['uses' => 'DepartmentController@getDepartmentList', 'as' => 'get.department.list']);
    Route::get('/department/edit/{id}', ['uses' => 'DepartmentController@getEditDepartment', 'as' => 'get.department.edit']);
    Route::post('/department/edit/', ['uses' => 'DepartmentController@postEditDepartment', 'as' => 'post.department.edit']);
    Route::get('/department/delete/{id}', ['uses' => 'DepartmentController@deleteDepartment', 'as' => 'get.department.delete']);
    Route::get('/employee/create', ['uses' => 'EmployeeController@getIndex', 'as' => 'get.employee.create']);
    Route::post('/employee/create', ['uses' => 'EmployeeController@postEmployee', 'as' => 'post.employee.create']);
    Route::get('/employee/list', ['uses' => 'EmployeeController@getEmmployeeList', 'as' => 'get.employee.list']);
    Route::get('/employee/edit/{id}', ['uses' => 'EmployeeController@getEditEmployee', 'as' => 'get.employee.edit']);
    Route::post('/employee//', ['uses' => 'EmployeeController@postEditEmployee', 'as' => 'post.employee.edit']);
    Route::get('/employee/delete/{id}', ['uses' => 'EmployeeController@deleteEmployee', 'as' => 'get.employee.delete']);
    Route::get('/holiday/create', ['uses' => 'HolidayController@getIndex', 'as' => 'get.holiday.create']);
    Route::post('/holiday/create', ['uses' => 'HolidayController@postHoliday', 'as' => 'post.holiday.create']);
    Route::get('/holiday/list', ['uses' => 'HolidayController@getHolidayList', 'as' => 'get.holiday.list']);
    Route::get('/holiday/edit/{id}', ['uses' => 'HolidayController@getEditHoliday', 'as' => 'get.holiday.edit']);
    Route::post('/holiday/edit/', ['uses' => 'HolidayController@postEditHoliday', 'as' => 'post.holiday.edit']);
    Route::get('/holiday/delete/{id}', ['uses' => 'HolidayController@deleteHoliday', 'as' => 'get.holiday.delete']);
    Route::get('/leave-req/list', ['uses' => 'LeaveController@getIndex', 'as' => 'get.leave.list']);
  });
});
